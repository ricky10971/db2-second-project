// number of competition for each winter sport

MATCH (c:Competition)-[]->(:OlympicGame{season:'Winter'})
WITH DISTINCT c
MATCH (c)-[]->(s:Sport)
RETURN s.name, count(c) as competitions
ORDER BY competitions DESC
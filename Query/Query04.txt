//Return the duration in days of each olympic games with its own year-season

MATCH (og:OlympicGame)
RETURN COALESCE(og.year + " "+ og.season) AS OlympicGames, duration.inDays(og.initialDate , og.finalDate).days AS durationInDays;
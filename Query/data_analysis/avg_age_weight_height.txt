// list of average values of age, height and weight for each sport

MATCH (s:Sport)<-[]-(a:Athlete)
WHERE a.age IS NOT NULL AND a.height IS NOT NULL AND a.weight IS NOT NULL
RETURN DISTINCT s.name AS sport, round(avg(a.age), 2) AS avg_age, round(avg(a.height), 2) AS avg_height, round(avg(a.weight), 2) AS avg_weight